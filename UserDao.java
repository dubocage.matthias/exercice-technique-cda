import java.util.List;

public interface UserDao {
    public void create(User user);
    public void updateUser(User user);
    public void deleteUser(User user);
    /*-------------------------------*/
    /*-----------Optionnel-----------*/
    /*-------------------------------*/
    public User getUser(Long id);
}