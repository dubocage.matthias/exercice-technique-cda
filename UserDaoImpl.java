import java.util.List;
import java.sql.*;

public class UserDaoImpl implements UserDao {

    @Override
    public void create(User user){
        String requete = "INSERT INTO user " +
                   "VALUES ("   + user.getID()+         ","
                                + user.getEmail()+      ","
                                + user.getBirthDate()+  ","
                                + user.getPassword()+   ")";
        try {
            Statement stmt = MySqlServConnection.getInstance().createStatement();
            stmt.executeUpdate(requete);
        }catch(SQLException se){
            se.printStackTrace();
        }
    }

    @Override
    public void updateUser(User user){
        String requete =   "UPDATE user " +
                    "SET email = " + user.getEmail()+
                    "SET birthdate = " + user.getBirthDate()+
                    "SET password = " + user.getPassword()+
                    "WHERE id =" + user.getID();
        try {
            Statement stmt = MySqlServConnection.getInstance().createStatement();
            stmt.executeUpdate(requete);
        }catch(SQLException se){
            se.printStackTrace();
        }

    }

    @Override
    public void deleteUser(User user){
        String requete = "DELETE FROM user WHERE id =" + user.getID();
        try {
            Statement stmt = MySqlServConnection.getInstance().createStatement();
            stmt.executeUpdate(requete);
        }catch(SQLException se){
            se.printStackTrace();
        }
    }

    
    /*-------------------------------*/
    /*-----------Optionnel-----------*/
    /*-------------------------------*/
    @Override
    public User getUser(Long id){
        String requete = "SELECT * FROM user WHERE id =" + id;
        User user;
        try {
            Statement stmt = MySqlServConnection.getInstance().createStatement();
            ResultSet rs = stmt.executeQuery(requete);
            user = new User(rs.getLong("id"),rs.getString("email"),rs.getString("password"),rs.getDate("birthdate"));
        }catch(SQLException se){
            se.printStackTrace();
            user = new User();
        }
        return user;
    }
}