import java.sql.*;

public class MySqlServConnection{
    private String url = "jdbc:postgresql://localhost:...";
    private String user = "root";
    private String password = "";
    private static Connection connection;

    private MySqlServConnection(){
          try {
              connection = DriverManager.getConnection(this.url, this.user, this.password);
          } catch (SQLException e) {
              e.printStackTrace();
          }
      }

    public static Connection getInstance(){
        if(connection == null){
            new MySqlServConnection();
        }
         return connection;   
    }   
}