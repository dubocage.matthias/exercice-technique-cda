import java.util.Date;
import java.util.List;

public class User{
    private Long id;
    private String email;
    private String password; 
    private Date birthDate;
    private List<Topic> topics;
    private List<Post> posts;

    /*------------------------------------*/
    /*-------------Optionnel--------------*/
    /*------------------------------------*/
    public User(){}

    public User(Long id,String email,String password, Date birthDate){
        this.id = id;
        this.email = email;
        this.password = password; 
        this.birthDate = birthDate;
    }

    //GETTER
    public String getEmail(){
        return this.email;
    }
    public Date getBirthDate(){
        return this.birthDate;
    }
    public Long getID(){
        return this.id;
    }
    public String getPassword(){
        return this.password;
    }
    public List<Post> getPosts(){
        return this.posts;
    }
    public List<Topic> getTopics(){
        return this.topics;
    }


    //SETTER
    public void setEmail(String newEmail){
        this.email = newEmail;
    }
    public void setBirthDate(Date newDate){
        this.birthDate = newDate;
    }
    public void setID(Long newID){
        this.id = newID;
    }
    public void  setPassword(String newPassword){
        this.password = newPassword;
    }
    public void setPosts(List<Post> newPosts){
        this.posts = newPosts;
    }
    public void setTopics(List<Topic> newTopics){
        this.topics = newTopics;
    }

}