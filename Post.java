import java.util.Date;
import java.util.List;

public class Post{
    private Long id;
    private Date postDate;
    private String content;
    private Topic topic;
    private User user;

    /*------------------------------------*/
    /*-------------Optionnel--------------*/
    /*------------------------------------*/
    public Post(){}

    public Post(Topic topic,User user , String content , Date postDate , Long id){
        this.user = user;
        this.topic = topic;
        this.id = id;
        this.postDate = postDate;
        this.content = content;
    }

    //GETTER
    public String getContent(){
        return this.content;
    }
    public Date getPostDate(){
        return this.postDate;
    }
    public Long getID(){
        return this.id;
    }
    public User getUser(){
        return this.user;
    }
    public Topic getTopic(){
        return this.topic;
    }


    //SETTER
    public void setContent(String newContent){
        this.content = newContent;
    }
    public void setBirthDate(Date newDate){
        this.postDate = newDate;
    }
    public void setID(Long newID){
        this.id = newID;
    }
    public void setUser(User newUser){
        this.user = newUser;
    }
    public void setTopic(Topic newTopic){
        this.topic= newTopic;
    }
}