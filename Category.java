import java.util.List;

public class Category{
    private Long id;
    private String label;
    private List<Topic> topics;
    
    /*------------------------------------*/
    /*-------------Optionnel--------------*/
    /*------------------------------------*/

    public Category(){}

    public Category(Long id,String label){
        this.id =id;
        this.label = label;
    }

    //Getter
    public Long getID(){
        return this.id;
    }
    public String getLabel(){
        return this.label;
    }
    public List<Topic> getTopics(){
        return this.topics;
    }

    //Setter
    public void setID(Long newID){
        this.id = newID;
    }
    public void setLabel(String newLabel){
        this.label= newLabel;
    }
    public void setTopics(List<Topic> newTopic){
        this.topics = newTopic;
    }
}