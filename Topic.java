import java.util.Date;
import java.util.List;

public class Topic{
    private Long id;
    private String title;
    private User user;
    private Category category;
    private List<Post> posts;

    /*------------------------------------*/
    /*-------------Optionnel--------------*/
    /*------------------------------------*/

    public Topic(){}

    public Topic(Long id,String title, User user, Category category){
        this.id = id;
        this.title = title;
        this.user = user;
        this.category = category;
    }

    //Getter
    public Long getID(){
        return this.id;
    }
    public String getTitle(){
        return this.title;
    }
    public User getUser(){
        return this.user;
    }
    public Category getCategory(){
        return this.category;
    }
    public List<Post> getPosts(){
        return this.posts;
    }

    //Setter
    public void setID(Long newID){
        this.id = newID;
    }
    public void setTitle(String newTitle){
        this.title = newTitle;
    }
    public void setUser(User newUser){
        this.user = newUser;
    }
    public void setCategory(Category newCategory){
        this.category = newCategory;
    }
    public void setPosts(List<Post> newPosts){
        this.posts = newPosts;
    }
}